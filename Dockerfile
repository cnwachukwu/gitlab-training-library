# Add dockerfile with correct packages for a yarn project
FROM mhart/alpine-node:10

# Add git to build git dependency sub modules
RUN apk add --no-cache git
